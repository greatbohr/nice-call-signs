import { countSyllables } from './syllableCount';

describe('syllableCount', () => {
    test.each([
        ['агава', 3],
        ['бор', 1],
        ['Ягода', 3],
        ['Ёлка', 2],
        ['жпчшц', 0],
        ['эму', 2],
        ['вымя', 2],
        ['Юпитер', 3]
    ])('%s should contain %i syllable', (word: string, numberOfSyllables: number) => {
        expect(countSyllables(word)).toBe(numberOfSyllables);
    });
});
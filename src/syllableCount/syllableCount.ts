import { RUS_VOVELS_STRING } from '../constants';

export const countSyllables = (word: string) => {
    const notVovelsRegEx = new RegExp(`[^${RUS_VOVELS_STRING}]`, 'ig');

    return word.replace(notVovelsRegEx, '').length;
}